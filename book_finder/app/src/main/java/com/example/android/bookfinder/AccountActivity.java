package com.example.android.bookfinder;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

public class AccountActivity extends AppCompatActivity {
    private SQLiteHelper MyDataBase;
    public DrawerLayout MyDrawerLayout;
    public ActionBarDrawerToggle MyActionBarDrawerToggle;
    private Book book;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        // drawer layout instance to toggle the menu icon to open and back button to close drawer
        MyDrawerLayout = findViewById(R.id.my_drawer_layout);
        MyActionBarDrawerToggle = new ActionBarDrawerToggle(this, MyDrawerLayout, R.string.nav_open, R.string.nav_close);
        // pass the Open and Close toggle for the drawer layout listener
        // to toggle the button
        MyDrawerLayout.addDrawerListener(MyActionBarDrawerToggle);
        MyActionBarDrawerToggle.syncState();
        // to make the Navigation drawer icon always appear on the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        MyDataBase = new SQLiteHelper(this);
        Intent data = getIntent();

        String title = data.getStringExtra("Title");
        String author = data.getStringExtra("Author");
        String description = data.getStringExtra("Description");
        String image = data.getStringExtra("Image");
        final String infolink = data.getStringExtra("infoLink");
        String publisher = data.getStringExtra("publisher");
        String publishedDate = data.getStringExtra("publishDate");
        final String webReaderLink = data.getStringExtra("webReaderLink");

        setTitle(title);

        this.book = new Book(title, author, infolink, image, description, publisher, publishedDate, webReaderLink, 0);
    }

    //the empty likelist button function
    public void emptyMyBook(View view) {
        MyDataBase.deleteAll();
        Toast.makeText(this, "My History empty finished", Toast.LENGTH_SHORT).show();
    }

    public void emptyLikeList(View view) {
        MyDataBase.deletaList("likedlist");
        Toast.makeText(this, "User Favourite books empty finished", Toast.LENGTH_SHORT).show();
    }

    public void emptyHistory(View view) {
        MyDataBase.deletaList("historylist");
        Toast.makeText(this, "User Browsing History empty finished", Toast.LENGTH_SHORT).show();
    }

    //shown my account page layout
    public void account(MenuItem item) {

    }

    public void likedlist(MenuItem item) {
        Intent l = new Intent(this, likedlistActivity.class);
        this.startActivity(l);
    }

    //shown history page layout
    public void historyList(MenuItem item) {
        Intent hList = new Intent(this, HistoryListActivity.class);
        this.startActivity(hList);
    }

    public void search(MenuItem item) {
        Intent MainActivity = new Intent(this, MainActivity.class);
        this.startActivity(MainActivity);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (MyActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }
}