package com.example.android.bookfinder;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

public class InfoActivity extends AppCompatActivity {

    private SQLiteHelper database;
    private boolean check_liked;
    private Book book;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);

        Intent data = getIntent();

        String title = data.getStringExtra("Title");
        String author = data.getStringExtra("Author");
        String description = data.getStringExtra("Description");
        String image = data.getStringExtra("Image");
        final String infolink = data.getStringExtra("infoLink");
        String publisher = data.getStringExtra("publisher");
        String publishedDate = data.getStringExtra("publishDate");
        final String webReaderLink = data.getStringExtra("webReaderLink");
        int pages = data.getIntExtra("pages", 0);

        setTitle(title);


        this.book = new Book(title, author, infolink, image, description, publisher, publishedDate, webReaderLink, 0);

        database = new SQLiteHelper(this);
        this.check_liked = database.checkBook("class175", "likedlist", title);
        TextView likeTextView = findViewById(R.id.like);
        if (this.check_liked) {
            likeTextView.setText("remove from liked list");
        } else {
            likeTextView.setText("add to liked list");
        }

        boolean check_history = database.checkBook("class175", "historylist", title);
        if (!check_history) {
            database.addOne(MainActivity.userID, book, "historylist");
        }

        ImageView imageView = findViewById(R.id.imageview);

        if (image != "") {
            Glide.with(this)
                    .load(image)
                    .into(imageView);
        } else {
            imageView.setImageResource(R.drawable.bookicon);
        }

        TextView titleTextView = findViewById(R.id.title);
        titleTextView.setText(title);

        TextView authorTextView = findViewById(R.id.author);
        authorTextView.setText(author);

        TextView publisherTextView = findViewById(R.id.publisher);
        publisherTextView.setText(publisher);

        TextView publishedDateTextView = findViewById(R.id.date);
        publishedDateTextView.setText(publishedDate);

        TextView pagesTextView = findViewById(R.id.pages);
        if (pages == 0) {
            pagesTextView.setText("Info not available.");
        } else {
            pagesTextView.setText(String.valueOf(pages));
        }

        TextView descriptionTextView = findViewById(R.id.description);
        descriptionTextView.setText(description);

        TextView infoLinkTextView = findViewById(R.id.infolink);
        infoLinkTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri bookUri = Uri.parse(infolink);

                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, bookUri);

                startActivity(websiteIntent);

            }
        });

        TextView webReaderLinkTextView = findViewById(R.id.readerlink);
        webReaderLinkTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (webReaderLink != "") {
                    Uri readerUri = Uri.parse(webReaderLink);

                    Intent websiteIntent = new Intent(Intent.ACTION_VIEW, readerUri);

                    startActivity(websiteIntent);
                } else {
                    Toast.makeText(getBaseContext(), "Web Link Unavailable", Toast.LENGTH_SHORT);
                }
            }
        });
    }

    public void like(View v) {
        if (this.check_liked) {
            database.deleteOne(MainActivity.userID, this.book, "likedlist");
            Intent likedlist = new Intent(this, likedlistActivity.class);
            this.startActivity(likedlist);
        } else {
            database.addOne(MainActivity.userID, this.book, "likedlist");
            Intent search = new Intent(this, MainActivity.class);
            this.startActivity(search);
        }
    }
}
