package com.example.android.bookfinder;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import java.util.ArrayList;

public class likedlistActivity extends AppCompatActivity {
    private BookAdapter mAdapter;

    public DrawerLayout drawerLayout;
    public ActionBarDrawerToggle actionBarDrawerToggle;
    private SQLiteHelper database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.likedlist);

        // drawer layout instance to toggle the menu icon to open
        // drawer and back button to close drawer
        drawerLayout = findViewById(R.id.my_drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.nav_open, R.string.nav_close);
        // pass the Open and Close toggle for the drawer layout listener
        // to toggle the button
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        // to make the Navigation drawer icon always appear on the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        database = new SQLiteHelper(this);
        SQLiteDatabase db = database.getWritableDatabase();
        ArrayList<Book> books = (ArrayList<Book>) database.getList("class175", "likedlist");

        mAdapter = new BookAdapter(this, books);
        ListView listView = findViewById(R.id.list);
        listView.setAdapter(mAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Book currentBook = mAdapter.getItem(position);

                String bookTitle = currentBook.getTitle();
                String bookAuthor = currentBook.getAuthor();
                String bookDescription = currentBook.getDescription();
                String bookImage = currentBook.getImageUrl();
                String bookLink = currentBook.getUrl();
                String bookPublisher = currentBook.getPublisher();
                String bookPublishDate = currentBook.getPublishedDate();
                String bookWebReaderLink = currentBook.getWebReaderLink();
                int bookPages = currentBook.getPages();

                Intent intent = new Intent(likedlistActivity.this, InfoActivity.class);
                intent.putExtra("Title", bookTitle);
                intent.putExtra("Author", bookAuthor);
                intent.putExtra("Description", bookDescription);
                intent.putExtra("Image", bookImage);
                intent.putExtra("infoLink", bookLink);
                intent.putExtra("publisher", bookPublisher);
                intent.putExtra("publishDate", bookPublishDate);
                intent.putExtra("webReaderLink", bookWebReaderLink);
                intent.putExtra("pages", bookPages);

                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.page_empty, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return super.onOptionsItemSelected(item);
        }
        switch (item.getItemId()) {
            case R.id.clear:
                database.deletaList("likedlist");
                mAdapter = new BookAdapter(this, new ArrayList<Book>());
                ListView listView = findViewById(R.id.list);
                listView.setAdapter(mAdapter);
                Toast.makeText(this, "User Favourite books empty finished", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    public void likedlist(MenuItem item) {

    }

    //shown history page layout
    public void historyList(MenuItem item) {
        Intent hList = new Intent(this, HistoryListActivity.class);
        this.startActivity(hList);
    }

    public void search(MenuItem item) {
        Intent MainActivity = new Intent(this, MainActivity.class);
        this.startActivity(MainActivity);
    }

    //shown my account page layout
    public void account(MenuItem item) {
        Intent AccountActivity = new Intent(this, AccountActivity.class);
        this.startActivity(AccountActivity);
    }
}
